\context Staff = "guia" \with {
	\consists Ambitus_engraver
	fontSize = #-3
	\override StaffSymbol #'staff-space = #(magstep -3)
} <<
	\set Staff.instrumentName = "Voz"
	\set Staff.shortInstrumentName = ""
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "guia" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key c \major

		R2*4  |
%% 5
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
		g' 8 e' 4. ~  |
		e' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
%% 10
		a' 8 a' g' f' 16 g' ~  |
		g' 2 ~  |
		g' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
%% 15
		g' 8 e' 4. ~  |
		e' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
		g' 2 ~  |
%% 20
		g' 4.. r16  |
		r8 c'' b' a' 16 g' ~  |
		g' 16 e' 4. r16  |
		r8 a' g' f' 16 g' ~  |
		g' 4.. r16  |
%% 25
		r8 c'' b' a' 16 g' ~  |
		g' 16 e' 4. r16  |
		f' 8 f' e' d'  |
		g' 2 ~  |
		g' 4 r  |
%% 30
		e' 4.. g' 16 ~  |
		g' 4.. r16  |
		a' 4 a' 8 g' ~  |
		g' 4.. r16  |
		r8 a' a' a' 16 g' ~  |
%% 35
		g' 8 e' d' c'  |
		d' 4 d'  |
		g' 4.. r16  |
		e' 4.. g' 16 ~  |
		g' 4.. r16  |
%% 40
		a' 4 a' 8 g' ~  |
		g' 4.. r16  |
		r8 a' a' a' 16 g' ~  |
		g' 8 e' d' c'  |
		d' 4 d'  |
%% 45
		c' 2 ~  |
		c' 4.. r16  |
		R2*2  |
		r8 g' g' g' 16 a' ~  |
%% 50
		a' 8 a' g' f' 16 g' ~  |
		g' 8 e' 4. ~  |
		e' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
%% 55
		g' 2 ~  |
		g' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
		g' 8 e' 4. ~  |
%% 60
		e' 4.. r16  |
		r8 g' g' g' 16 a' ~  |
		a' 8 a' g' f' 16 g' ~  |
		g' 2 ~  |
		g' 4.. r16  |
%% 65
		r8 c'' b' a' 16 g' ~  |
		g' 16 e' 4. r16  |
		r8 a' g' f' 16 g' ~  |
		g' 4.. r16  |
		r8 c'' b' a' 16 g' ~  |
%% 70
		g' 16 e' 4 r16 e' 8  |
		f' 8 f' e' d'  |
		g' 2 ~  |
		g' 4 r  |
		e' 4.. g' 16 ~  |
%% 75
		g' 4.. r16  |
		a' 4 a' 8 g' ~  |
		g' 4.. r16  |
		r8 a' a' a' 16 g' ~  |
		g' 8 e' d' c'  |
%% 80
		d' 4 d'  |
		g' 4.. r16  |
		e' 4.. g' 16 ~  |
		g' 4.. r16  |
		a' 4 a' 8 g' ~  |
%% 85
		g' 4.. r16  |
		r8 a' a' a' 16 g' ~  |
		g' 8 e' d' c'  |
		d' 4 d'  |
		c' 2 ~  |
%% 90
		c' 4.. r16  |
		R2*6  |
		\bar "|."
	}

	\new Lyrics \lyricsto "guia" {
		Que nues -- tras ma __ nos fa -- ti -- ga __ das, __
		pron -- to "se han" de __ for -- ta -- le -- cer; __
		que las ro -- di __ llas va -- ci -- lan __ tes __
		"se a" -- fian -- cen con __ se -- gu -- ri -- dad. __

		"Tú has" pro -- me -- ti __ do
		un Sal -- va -- dor. __
		"Tú has" a -- nun -- cia __ do
		la li -- be -- ra -- ción. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue __ blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue __ blo cla -- ma li -- ber -- tad. __

		Los o -- jos de __ los hom -- bres cie __ gos __ 
		en -- ton -- ces "se i" __ lu -- mi -- na -- rán, __
		y los o -- í __ dos de los sor __ dos __
		ya con sor -- pre __ sa "se a" -- bri -- rán. __
		Los co -- jos an __ dan,
		los cie -- gos ven, __
		los sor -- dos o __ yen,
		el mu -- do can -- ta -- rá. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue __ blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue __ blo cla -- ma li -- ber -- tad. __
	}
>>
