\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key c \major

		R2*4  |
%% 5
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
		e 4.. r16  |
		r8 g g g 16 a ~  |
%% 10
		a 8 a g f 16 g ~  |
		g 2 ~  |
		g 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
%% 15
		g 8 e 4. ~  |
		e 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 2 ~  |
%% 20
		g 4.. r16  |
		r8 c' b a 16 g ~  |
		g 16 e 4. r16  |
		r8 a g f 16 g ~  |
		g 4.. r16  |
%% 25
		r8 c' b a 16 g ~  |
		g 16 e 4. r16  |
		f 8 f e d  |
		g 2 ~  |
		g 4 r  |
%% 30
		e 4.. g 16 ~  |
		g 4.. r16  |
		a 4 a 8 g ~  |
		g 4.. r16  |
		r8 a a a 16 g ~  |
%% 35
		g 8 e d c  |
		d 4 d  |
		g 4.. r16  |
		r4 c' 8. b 16 ~  |
		b 4.. r16  |
%% 40
		c' 4 c' 8 b ~  |
		b 4.. r16  |
		r8 c' c' c' 16 c' ~  |
		c' 8 c' c' c'  |
		b 4 b  |
%% 45
		c' 2 ~  |
		c' 4.. r16  |
		R2*2  |
		r8 g g g 16 a ~  |
%% 50
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
		e 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
%% 55
		g 2 ~  |
		g 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
%% 60
		e 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 2 ~  |
		g 4.. r16  |
%% 65
		r8 c' b a 16 g ~  |
		g 16 e 4. r16  |
		r8 a g f 16 g ~  |
		g 4.. r16  |
		r8 c' b a 16 g ~  |
%% 70
		g 16 e 4 r16 e 8  |
		f 8 f e d  |
		g 2 ~  |
		g 4 r  |
		e 4.. g 16 ~  |
%% 75
		g 4.. r16  |
		a 4 a 8 g ~  |
		g 4.. r16  |
		r8 a a a 16 g ~  |
		g 8 e d c  |
%% 80
		d 4 d  |
		g 4.. r16  |
		r4 c' 8. b 16 ~  |
		b 4.. r16  |
		c' 4 c' 8 b ~  |
%% 85
		b 4.. r16  |
		r8 c' c' c' 16 c' ~  |
		c' 8 c' c' c'  |
		b 4 b  |
		c' 2 ~  |
%% 90
		c' 4.. r16  |
		R2*2  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
%% 95
		g 8 e 4. ~  |
		e 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 2 ~  |
%% 100
		g 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
		e 4.. r16  |
%% 105
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 2 ~  |
		g 4.. r16  |
		r8 c' b a 16 g ~  |
%% 110
		g 16 e 4. r16  |
		r8 a g f 16 g ~  |
		g 4.. r16  |
		r8 c' b a 16 g ~  |
		g 16 e 4. r16  |
%% 115
		f 8 f e d  |
		g 2 ~  |
		g 4 r  |
		e 4.. g 16 ~  |
		g 4.. r16  |
%% 120
		a 4 a 8 g ~  |
		g 4.. r16  |
		r8 a a a 16 g ~  |
		g 8 e d c  |
		d 4 d  |
%% 125
		g 4.. r16  |
		r4 c' 8. b 16 ~  |
		b 4.. r16  |
		c' 4 c' 8 b ~  |
		b 4.. r16  |
%% 130
		r8 c' c' c' 16 c' ~  |
		c' 8 c' c' c'  |
		b 4 b  |
		c' 2 ~  |
		c' 4.. r16  |
%% 135
		R2*2  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
%% 140
		e 4.. r16  |
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 2 ~  |
		g 4.. r16  |
%% 145
		r8 g g g 16 a ~  |
		a 8 a g f 16 g ~  |
		g 8 e 4. ~  |
		e 4.. r16  |
		r8 g g g 16 a ~  |
%% 150
		a 8 a g f 16 g ~  |
		g 2 ~  |
		g 4.. r16  |
		r8 c' b a 16 g ~  |
		g 16 e 4. r16  |
%% 155
		r8 a g f 16 g ~  |
		g 4.. r16  |
		r8 c' b a 16 g ~  |
		g 16 e 4 r16 r8  |
		f 8 f e d  |
%% 160
		g 2 ~  |
		g 4 r  |
		e 4.. g 16 ~  |
		g 4.. r16  |
		a 4 a 8 g ~  |
%% 165
		g 4.. r16  |
		r8 a a a 16 g ~  |
		g 8 e d c  |
		d 4 d  |
		g 4.. r16  |
%% 170
		r4 c' 8. b 16 ~  |
		b 4.. r16  |
		c' 4 c' 8 b ~  |
		b 4.. r16  |
		r8 c' c' c' 16 c' ~  |
%% 175
		c' 8 c' c' c'  |
		b 4 b  |
		c' 2 ~  |
		c' 4.. r16  |
		R2*4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Que nues -- tras ma -- nos fa -- ti -- ga -- das, __
		pron -- to "se han" de __ for -- ta -- le -- cer; __
		que las ro -- di -- llas va -- ci -- lan -- tes __
		"se a" -- fian -- cen con __ se -- gu -- ri -- dad. __

		"Tú has" pro -- me -- ti -- do
		un Sal -- va -- dor. __
		"Tú has" a -- nun -- cia -- do
		la li -- be -- ra -- ción. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad. __

		Los o -- jos de __ los hom -- bres cie -- gos __
		en -- ton -- ces "se i" -- lu -- mi -- na -- rán, __
		y los o -- í -- dos de los sor -- dos __
		ya con sor -- pre -- sa "se a" -- bri -- rán. __
		Los co -- jos an -- dan,
		los cie -- gos ven, __
		los sor -- dos o -- yen,
		el mu -- do can -- ta -- rá. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad. __

		Que to -- do va -- lle se le -- van -- te, __
		es -- tén los mon -- tes por ba -- jar; __
		que lo tor -- ci -- do "se en" -- de -- re -- ce, __
		y "lo es" -- ca -- bro -- "so has" "de a" -- lla -- nar; __
		pues los ca -- mi -- nos "se a" -- lis -- ta -- rán, __
		nues -- tro con -- sue -- lo pron -- to lle -- ga -- rá. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad. __

		Que su -- "ba al" mon -- "te el" men -- sa -- je -- ro, __
		des -- de lo al -- "to al" -- ce su voz; __
		sin mie -- do gri -- ta, pre -- go -- ne -- ro, __
		que sus no -- ti -- cias bue -- nas son. __
		"Se a" -- cer -- "ca el" dí -- a en "que el" pas -- tor, __
		a su re -- ba -- ño a -- pa -- cen -- ta -- rá. __

		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad.
		Ven, ven, __ ven, Se -- ñor, __
		ven que tu pue -- blo cla -- ma li -- ber -- tad. __
	}
>>
