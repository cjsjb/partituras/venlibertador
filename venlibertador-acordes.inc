\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	c2 bes8. f8. ~ f8
	c2 bes8. f8. ~ f8

	% [1] que nuestras manos fatigadas...
	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	% tu has prometido...
	a4..:m e16:m ~ e2:m
	f2 c2
	a4..:m e16:m ~ e2:m
	f2 g2 g2

	% ven, ven, ven, sennor...
	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 d2 g2

	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 g2

	% glue
	c2 bes8. f8. ~ f8
	c2 bes8. f8. ~ f8

	% [2] los ojos de los hombres ciegos...
	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	% los cojos andan...
	a4..:m e16:m ~ e2:m
	f2 c2
	a4..:m e16:m ~ e2:m
	f2 g2 g2

	% ven, ven, ven, sennor...
	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 d2 g2

	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 g2

	% glue
	c2 bes8. f8. ~ f8
	c2 bes8. f8. ~ f8

	% [3] que todo valle se levante...
	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	% pues los caminos...
	a4..:m e16:m ~ e2:m
	f2 c2
	a4..:m e16:m ~ e2:m
	f2 g2 g2

	% ven, ven, ven, sennor...
	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 d2 g2

	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 g2

	% glue
	c2 bes8. f8. ~ f8
	c2 bes8. f8. ~ f8

	% [4] que suba al monte el mensajero...
	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	c4.. f16 ~ f2
	c2 bes8. f8. ~ f8
	c4.. f16 ~ f2
	g2 g2

	% pues los caminos...
	a4..:m e16:m ~ e2:m
	f2 c2
	a4..:m e16:m ~ e2:m
	f2 g2 g2

	% ven, ven, ven, sennor...
	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 d2 g2

	c4.. g16 ~ g2
	a4..:m e16:m ~ e2:m
	f2 c2 g2

	% outro
	c2 bes8. f8. ~ f8
	c2 bes8. f8. ~ f8
	c2 ~ c2 ~ c2
	}
